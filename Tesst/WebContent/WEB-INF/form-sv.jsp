<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Form</title>
</head>
<body>
	<h1 style="background-color: grey; text-align: center;">Student
		Form</h1>
	<div style="display: flex; justify-content: center; align-items: center;">
		<form action="show" method="post">
			<table>
				<tr>
					<td>Student Name</td>
					<td><input type="text" name="stuName" /></td>
				</tr>
				<tr>
					<td>Student ID</td>
					<td><input type="text" name="stuId" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit"
						style="margin-left: 100px;" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>