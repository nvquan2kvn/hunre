<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>View</title>
</head>
<body>
	<h1 style="text-align: center;">List Student</h1>
		<p style="text-align: center; color: blue;">${msg}</p>
	<div style="display: flex; justify-content: center; align-items: center;">
		<table border="1">
			<tr>
				<td style="background-color: #8080805e;">Student Name</td>
				<td style="color: pink;">${s.stuName}</td>
			</tr>
			<tr>
				<td style="background-color: #8080805e;">Student ID</td>
				<td style="color: pink;">${s.stuId}</td>
			</tr>
		</table>
	</div>
	<a href="home" style="text-align: center;">Back</a>
</body>
</html>