
package com.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentController {
	@RequestMapping(value = {"","/home"})
	public ModelAndView formSV() {
		ModelAndView model = new ModelAndView("form-sv");
		return model;
	}
	@RequestMapping(value = "/showSV")
	public ModelAndView showSV(@RequestParam("stuName") String stuName, @RequestParam("stuId") String stuId) {
		Student stu = new Student();
		stu.setStuName(stuName);
		stu.setStuId(stuId);
		ModelAndView model = new ModelAndView("view-sv");
		model.addObject("msg", "Student DH8C5");
		model.addObject("s", stu);
		return model;
	}
	@RequestMapping(value = "show",method = RequestMethod.POST)
	public ModelAndView viewStu(@ModelAttribute("s") Student s) {
		ModelAndView model = new ModelAndView("view-sv");
		model.addObject("msg", "Student DH8C5");
		return model;
	}
}
